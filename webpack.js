module.exports = {
  mode: 'development',
  entry: __dirname + '/src/index.js', // webpack entry point. Module to start building dependency graph
  output: {
    path: __dirname + '/app/static', // Folder to store generated bundle
    filename: 'app.js', // Name of generated bundle after build
    publicPath: '/', // public URL of the output directory when referenced in a browser
  },
  module: {
    // where we defined file patterns and their loaders
    rules: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: [/node_modules/],
      },
    ],
  },
};
