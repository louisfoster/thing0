import Clock from '../clock/clock';
import InputController from '../input/inputController';
import InputKeyboard from '../input/inputKeyboard';
import Renderer from '../renderer/renderer';
import Scene from '../scene/scene';
import Debugger from './debugger';

export default class Main {
    constructor() {
        // Init entities

        this.clock = new Clock();
        this.inputController = new InputController();
        this.inputKeyboard = new InputKeyboard();
        this.renderer = new Renderer(this);
        this.scene = new Scene(
            this,
            this.renderer.gl.canvas.width,
            this.renderer.gl.canvas.height
        );
        this.debugger = new Debugger(this);

        // Entity storage

        this.entities = [
            this.clock,
            this.inputController,
            this.inputKeyboard,
            this.renderer,
            this.scene,
            this.debugger,
        ];

        // Bind methods

        // Primary update methods
        this.updateAll = this.updateAll.bind(this);

        // Primary game loop
        this.loop = this.loop.bind(this);

        // Entity filters
        this.getInputtableEntities = this.getInputtableEntities.bind(this);
        this.getDebuggableEntities = this.getDebuggableEntities.bind(this);

        // Scene's delegate
        this.sceneReady = this.sceneReady.bind(this);
        this.glContext = this.glContext.bind(this);

        // Renderer's delegate
        this.resizeEvent = this.resizeEvent.bind(this);
    }

    // Get all updatable?
    updateAll() {
        this.entities.forEach(el => {
            if (el.isUpdatable) el.update();
        });
    }

    loop() {
        requestAnimationFrame(this.loop);

        this.updateAll();

        // draw scene
        this.scene.draw();
    }

    // Entity type filter methods
    getDebuggableEntities() {
        return this.entities.filter(el => el.isDebuggable);
    }
    getInputtableEntities() {
        return this.entities.filter(el => el.isInputtable);
    }

    // Scene's delegate methods
    sceneReady() {
        this.loop();
    }

    glContext() {
        return this.renderer.gl;
    }

    // Renderer's delegate methods
    resizeEvent(newWidth, newHeight) {
        this.scene.resize(newWidth, newHeight);
    }
}
