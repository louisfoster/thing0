import { mat4 as m4, quat } from 'gl-matrix';

class SpatialVector {
    constructor(_x = 0.0, _y = 0.0, _z = 0.0) {
        this.x = _x;
        this.y = _y;
        this.z = _z;

        this.get = this.get.bind(this);
        this.set = this.set.bind(this);
    }

    get() {
        return [this.x, this.y, this.z];
    }

    set(_x, _y, _z) {
        this.x = _x || this.x;
        this.y = _y || this.y;
        this.z = _z || this.z;
    }
}

export class SpatialComponent {
    constructor(_delegate) {
        this.delegate = _delegate;
        this.position = new SpatialVector();
        this.rotation = new SpatialVector();
        this.scale = new SpatialVector(1.0, 1.0, 1.0);

        this.modelMatrix = this.modelMatrix.bind(this);
        this.localModelMatrix = this.localModelMatrix.bind(this);
        this.worldModelMatrix = this.worldModelMatrix.bind(this);
    }

    /**
     * Return the local model matrix for the component values
     */
    localModelMatrix() {
        return this.modelMatrix();
    }

    /**
     * Get the delegate (ie component's owner) offset, which generally
     * either points to it's parent's spatial component wMM method
     * or just returns an identity matrix
     */
    worldModelMatrix() {
        let matrix = this.delegate.offsetModelMatrix();
        m4.mul(matrix, matrix, this.localModelMatrix());
        return matrix;
    }

    modelMatrix() {
        let matrix = m4.create();
        let q = quat.create();
        quat.fromEuler(q, this.rotation.x, this.rotation.y, this.rotation.z);
        m4.fromRotationTranslationScale(
            matrix,
            q,
            this.position.get(),
            this.scale.get()
        );
        return matrix;
    }
}
