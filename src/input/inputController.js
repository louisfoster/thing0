import DebugComponent from '../components/debugComponent';

export default class InputController {
    constructor() {
        // Input types have "state" objects
        this.isInputtable = true;
        this.state = {};

        this.controller = navigator.getGamepads()[0];
        this.update = this.update.bind(this);
        this.isDebuggable = true;
        this.debug = new DebugComponent('Input Controller');
        this.isUpdatable = true;
    }
    update() {
        this.controller = navigator.getGamepads()[0];
        if (this.controller) {
            this.debug.clearText();
            this.debug.addLine(`ID: ${this.controller.id}`);

            this.debug.addLine('Buttons:');
            this.controller.buttons.forEach((el, i) => {
                let name = this.getControllerButtonName(i);
                this.state[name] = el.pressed;
                this.debug.addLine(`${name}: id(${i}) pressed(${el.pressed})`);
            });

            this.debug.addLine('Joysticks:');
            this.controller.axes.forEach((el, i) => {
                let name = this.getJoystickAxisName(i);
                this.state[name] = el;
                this.debug.addLine(`${name}: id(${i}) value(${el})`);
            });
        }
    }
    getControllerButtonName(id) {
        switch (id) {
            case 0:
                return 'A';
            case 1:
                return 'B';
            case 2:
                return 'X';
            case 3:
                return 'Y';
            case 4:
                return 'L1';
            case 5:
                return 'R1';
            case 6:
                return 'L2';
            case 7:
                return 'R2';
            case 8:
                return 'BACK';
            case 9:
                return 'START';
            case 10:
                return 'L3';
            case 11:
                return 'R3';
            case 12:
                return 'UP';
            case 13:
                return 'DOWN';
            case 14:
                return 'LEFT';
            case 15:
                return 'RIGHT';
        }
    }

    getJoystickAxisName(id) {
        switch (id) {
            case 0:
                return 'LX';
            case 1:
                return 'LY';
            case 2:
                return 'RX';
            case 3:
                return 'RY';
        }
    }
}
