import { mat4 as m4 } from 'gl-matrix';

import DebugComponent from '../components/debugComponent';
import { SpatialComponent } from '../components/spatialComponent';

import Camera from './camera/camera';
import Terrain from './objects/environment/terrain';
import Player from './objects/player/player';

export default class Scene {
    constructor(_delegate, viewWidth, viewHeight) {
        this.delegate = _delegate;

        this.isSpatial = true;
        this.spatial = new SpatialComponent(this);
        this.offsetModelMatrix = this.offsetModelMatrix.bind(this);

        this.player = new Player(this);
        this.player.camera = new Camera(this.player, viewWidth, viewHeight);
        this.getInputtableEntities = this.getInputtableEntities.bind(this);

        this.isUpdatable = true;
        this.update = this.update.bind(this);

        this.resize = this.resize.bind(this);

        this.isDebuggable = true;
        this.debug = new DebugComponent('Scene');

        this.draw = this.draw.bind(this);
        let gl = this.delegate.glContext();
        this.terrain = new Terrain(this, gl);
        this.terrainReady = this.terrainReady.bind(this);
        this.children = [this.terrain, this.player];
    }

    terrainReady() {
        this.delegate.sceneReady();
    }

    resize(newWidth, newHeight) {
        // For each child with a camera, resize camera?
        this.children.forEach(child => {
            if (child.camera) child.camera.resize(newWidth, newHeight);
        });
    }

    update() {
        // Update children, let children update their cameras
        this.children.forEach(child => {
            if (child.isUpdatable) child.update();
        });
    }

    draw() {
        let gl = this.delegate.glContext();
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.enable(gl.DEPTH_TEST);

        let projectionView = m4.create();
        m4.mul(
            projectionView,
            this.player.camera.projection,
            this.player.camera.view()
        );

        this.children.forEach(child => {
            if (child.isVisible) child.draw(gl, projectionView);
        });
    }

    /**
     * Player delegate methods
     */
    getInputtableEntities() {
        return this.delegate.getInputtableEntities();
    }

    /**
     * Spatial delegate methods
     */
    offsetModelMatrix() {
        return m4.create();
    }
}
