import { mat4 as m4 } from 'gl-matrix';

import { SpatialComponent } from '../../../components/spatialComponent';
import { setupShaders } from '../../shaders/shaders';

export default class Terrain {
    /**
     *   - Has general spatial info ie spatial component
     *   - has height map (matrix of normalised float between 0 and x)
     *   - has lighting (perhaps received from parent level lighting?)
     *   - has texture/color (firstly, the higher the lighter)
     *   - has normals:
     *  https://stackoverflow.com/questions/13983189/opengl-how-to-calculate-normals-in-a-terrain-height-grid
     *   - shaders to shade...
     */

    constructor(_delegate, gl) {
        /**
         *   Delegate:
         *   - terrainReady, for when the terrain is done instantiating (ready to
         * draw)
         */
        this.delegate = _delegate;

        this.isSpatial = true;
        this.spatial = new SpatialComponent(this);
        this.offsetModelMatrix = this.offsetModelMatrix.bind(this);
        this.spatial.position.set(-250, 2.5, -250); // pos y moves terrain down?
        this.spatial.rotation.set(90, 0, 0);
        this.spatial.scale.set(10, 10, 5);

        this.isVisible = true;
        this.draw = this.draw.bind(this);

        /**
         * Array matrix for height values of terrain
         */
        this.size = 50;
        this.heightMap = this.randomHeights(this.size);

        /**
         * build mesh (triangle strip), float32Array
         */
        this.mesh = this.buildMesh(this.size, this.heightMap);
        this.vertexCount = this.mesh.length / 3;
        this.program;
        this.setShaderData = this.setShaderData.bind(this);

        setupShaders(gl, 'terrain', 'scene', this, () => {
            this.setShaderData(gl);
        });
    }

    setShaderData(gl) {
        let vao = gl.createVertexArray();
        gl.bindVertexArray(vao);

        const vertexAttributeLocation = gl.getAttribLocation(
            this.program,
            'a_vertex'
        );
        const vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
        gl.bufferData(gl.ARRAY_BUFFER, this.mesh, gl.STATIC_DRAW);

        gl.enableVertexAttribArray(vertexAttributeLocation);
        gl.vertexAttribPointer(
            vertexAttributeLocation,
            3,
            gl.FLOAT,
            false,
            0,
            0
        );
        this.delegate.terrainReady();
    }

    /**
     * size x size number of random floats
     */
    randomHeights(size) {
        let n = size * size;
        let valueArray = [];
        for (let i = 0; i < n; i++) {
            valueArray.push(Math.random());
        }
        return valueArray;
    }

    buildMesh(size, heights) {
        /**
         * For x size grid
         * we calculate x - 2 rows
         * Getting row offset
         * Alternate between offset + column (x -1) and its following row (+x)
         * as long as it isn't the final row, we reference final point of row, and
         * the first point of the next row
         */

        let mesh = [];
        // Rows (up to size - 2)
        for (let row = 0; row < size - 1; row++) {
            // Column (up to size - 1)
            for (let col = 0; col < size; col++) {
                // coordinates, y is row, x is column, z is height
                // we retrieve the top verts of the row and then the bottom (y + size)
                let xTop = col;
                let yTop = row;
                let zTop = heights[size * yTop + xTop];
                let xBase = col;
                let yBase = row + 1;
                let zBase = heights[size * yBase + xBase];

                // Add values
                mesh.push(xTop, yTop, zTop, xBase, yBase, zBase);

                // are we at the last column (size - 1)?
                // // if so, are we NOT on the final row (size - 2)?
                if (col === size - 1 && row !== size - 2) {
                    // then, we should cap this off with the base again, and the first
                    // value of the next
                    mesh.push(xBase, yBase, zBase);
                    // col == 0, but row + 1....
                    let xNext = 0;
                    let yNext = yBase;
                    let zNext = heights[size * yNext];

                    mesh.push(xNext, yNext, zNext);
                }
            }
        }

        return new Float32Array(mesh);
    }

    draw(gl, projectionView) {
        // Use the program
        gl.useProgram(this.program);

        m4.mul(projectionView, projectionView, this.spatial.worldModelMatrix());
        let mvpUniform = gl.getUniformLocation(
            this.program,
            'modelViewProjectionMatrix'
        );
        gl.uniformMatrix4fv(mvpUniform, false, projectionView);

        // Draw a single triangle
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, this.vertexCount);
    }

    /**
     * Spatial component delegate methods
     */

    offsetModelMatrix() {
        /**
         * I guess if this had a parent, we could check for that and get it's world
         * matrix, or alternatively just return an identity
         */
        return this.delegate.spatial.worldModelMatrix();
    }
}
