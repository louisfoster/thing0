import { mat4, vec3, mat3, quat } from 'gl-matrix';
import { SpatialComponent } from '../../../components/spatialComponent';

/**
 * - spatial, located in 3D space
 * - physical, reacts to environment (base == height map point lerp) and
 * other things
 * - not visible
 * - has first person camera (player moves and rotates around up axis (yaw),
 * camera rotates along right axis (pitch))
 */

export default class Player {
    constructor(_delegate) {
        /**
         * Delegate handles:
         *  - getInputtableEntities()
         */
        this.delegate = _delegate;
        this.id = 'player';

        this.isSpatial = true;
        this.spatial = new SpatialComponent(this);
        this.offsetModelMatrix = this.offsetModelMatrix.bind(this);
        this.spatial.position.set(10, -5, 10);

        this.camera;
        this.getInputtableEntities = this.getInputtableEntities.bind(this);

        this.isUpdatable = true;
        this.update = this.update.bind(this);
    }

    update() {
        this.camera.update();
        let inputs = this.getInputtableEntities();
        inputs.forEach(el => {
            let m = mat4.create();
            let q = quat.create();
            quat.fromEuler(q, 0, this.camera.spatial.rotation.y, 0);
            mat4.fromRotationTranslation(m, q, this.spatial.position.get());

            /**
             * Movement joystick along y axis (up-down) ie dolly
             */
            let yAxisMove = el.state['LY'];
            if (yAxisMove !== 0 && Math.abs(yAxisMove) > 0.1) {
                mat4.translate(m, m, [0, 0, yAxisMove]);
            }
            /**
             * Movement joystick along x axis (left-right) ie truck
             */
            let xAxisMove = el.state['LX'];
            if (xAxisMove !== 0 && Math.abs(xAxisMove) > 0.1) {
                mat4.translate(m, m, [-1 * xAxisMove, 0, 0]);
            }

            let v = vec3.create();
            mat4.getTranslation(v, m);
            this.spatial.position.x = v[0];
            this.spatial.position.z = v[2];
        });
    }

    /**
     * Spatial component delegate methods
     */

    offsetModelMatrix() {
        /**
         * I guess if this had a parent, we could check for that and get it's world
         * matrix, or alternatively just return an identity
         */
        return this.delegate.spatial.worldModelMatrix();
    }

    /**
     * Position/Camera delegate methods
     */

    getInputtableEntities() {
        return this.delegate.getInputtableEntities();
    }
}
