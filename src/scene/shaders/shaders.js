function createShader(gl, sourceCode, type) {
    // Compiles either a shader of type gl.VERTEX_SHADER or gl.FRAGMENT_SHADER
    var shader = gl.createShader(type);
    gl.shaderSource(shader, sourceCode);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        var info = gl.getShaderInfoLog(shader);
        throw 'Could not compile WebGL program. \n\n' + info;
    }
    return shader;
}

function createShaderProgram(gl, vert, frag, target, done) {
    let vertexShader = createShader(gl, vert, gl.VERTEX_SHADER);
    let fragmentShader = createShader(gl, frag, gl.FRAGMENT_SHADER);

    var program = gl.createProgram();

    // Attach pre-existing shaders
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);

    gl.linkProgram(program);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        var info = gl.getProgramInfoLog(program);
        throw 'Could not compile WebGL program. \n\n' + info;
    }

    target.program = program;

    done();
}

export function setupShaders(
    gl,
    vertexSourceName,
    fragmentSourceName,
    target,
    done
) {
    let vert;
    let frag;

    fetch(`shaders/vertex/${vertexSourceName}.vert`)
        .then(res => res.text())
        .then(txt => {
            console.log(`Vert source:\n${txt}`);
            vert = txt;
            if (frag) createShaderProgram(gl, vert, frag, target, done);
        });

    fetch(`shaders/fragment/${fragmentSourceName}.frag`)
        .then(res => res.text())
        .then(txt => {
            console.log(`Frag source:\n${txt}`);
            frag = txt;
            if (vert) createShaderProgram(gl, vert, frag, target, done);
        });
}
