# thing0

## Setup

-   Requires a controller and driver (ensure it is connected and activated)

*   Install nginx
*   symlink nginx server dir to project's nginx conf file
*   Optional: Install node, run `npm install` and run `npm start` for dev or `npm build` for prod
*   Open browser to `localhost:1212`

## Info

-   Using a "sort of" ECS structure:
    -   If something requires all entities of a certain type, they generally contain a delegate reference (the delegate in most/all cases being the "main" class) which holds methods for filtering the entities of a given type (e.g. isInputtable)
    -   Input entities have `isInputtable == true` along with a `state` object to retrieve input values by their given name
    -   Entities to be updated per tick has `isUpdatable == true` and an `update()` function
    -   Entities with debug output have `isDebuggable == true` and an `debug` property referencing a `DebugComponent`, the debug component stores lines of debug text, which is general modified during an update tick.
    -   Entities that have 3D spatial data have `isSpatial == true` and hold spatial data in a `spatial` property referencing a `SpatialComponent` which allows for data to be retrieved via a `get()` method.

## TODO

-   need to figure out how to rotate around current world position (seems to be either move along world axis or rotate around world origin, but need to find world position before applying rotating but move to that position via the direction of the given rotation)
-   Change to typescript
-   All axis moving camera
-   Debug interface "register" for altering values
-   pass time delta to update functions
-   debounce resize and anything else needing debouncing...
-   nb. it might be worthwhile to ensure only valid keyboard keys are accessible to state

## Resources

-   https://github.com/360Controller/360Controller/releases
-   https://developer.mozilla.org/en-US/docs/Web/API/Gamepad_API/Using_the_Gamepad_API
-   https://github.com/toji/gl-matrix
-   https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API
-   http://www.corehtml5.com/trianglestripfundamentals.php
-   https://stackoverflow.com/questions/13983189/opengl-how-to-calculate-normals-in-a-terrain-height-grid
