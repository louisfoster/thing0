uniform mat4 modelViewProjectionMatrix;
attribute vec3 a_vertex;
varying vec4 v_color;

void main() {
  v_color = vec4(vec3(a_vertex.z), 1);
  float v_x = a_vertex.x;
  float v_y = a_vertex.y;
  float v_z = a_vertex.z;
  gl_Position = modelViewProjectionMatrix * vec4(v_x, v_y, v_z, 1);
}
