uniform mat4 modelViewProjectionMatrix;
attribute vec4 a_vertex;
// attribute vec4 a_color;
varying vec4 v_color;

void main() {
  // v_color = a_color;
  v_color = vec4(1, 1, 1, 1);
  gl_Position = modelViewProjectionMatrix * a_vertex;
}
